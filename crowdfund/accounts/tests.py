from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

class AccountsTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="test")

    def test_donations(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse("my-donations"))
        self.assertJSONEqual(response.content, [])