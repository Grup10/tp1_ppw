from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic

class Profile(LoginRequiredMixin, generic.TemplateView):
    template_name = 'accounts/profile.html'

@login_required
def donations(request):
    return JsonResponse([{
        'amount': donation.amount, 
        'program': donation.program.name
    } for donation in request.user.donations.all()], safe=False)