from django.apps import AppConfig


class AccountConfig(AppConfig):
    name = 'crowdfund.accounts'
