from django.shortcuts import render
from .models import Do
from .forms import Testi_Form

response = {}
def index(request):
	todo = Do.objects.all()
	response['todo'] = todo
	response['form'] = Testi_Form
	return render(request, "crowdfund/index.html", response)


# Create your views here.
