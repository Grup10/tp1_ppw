from django import forms

class Testi_Form(forms.Form):
	error_messages = {
		'required' : 'Please input this field',
	}
	mssg_status = {
		'type': 'text',
        'class': 'inputan',
        'placeholder':"Masukan testimoni anda mengenai website kami"

	}
	testimony = forms.CharField(label='', required=True, max_length=300, widget=forms.Textarea(attrs=mssg_status))