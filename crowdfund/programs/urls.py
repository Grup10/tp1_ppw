from django.urls import path
from . import views

urlpatterns = [
    path("", views.ProgramList.as_view(), name="programs"),
]
