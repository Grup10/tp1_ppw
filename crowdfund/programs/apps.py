from django.apps import AppConfig


class ProgramsConfig(AppConfig):
    name = 'crowdfund.programs'
