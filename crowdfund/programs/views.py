from django.views.generic import ListView
from .models import Program



class ProgramList(ListView):
    model = Program
    context_object_name = 'programs'


