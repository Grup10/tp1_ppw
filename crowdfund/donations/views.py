from django.views import View, generic
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from crowdfund.programs.models import Program
from .models import Donation
from .forms import DonationForm


class DonateNew(LoginRequiredMixin, View):
    def post(self, request, id):
        program = get_object_or_404(Program, id=id)
        form = DonationForm(request.POST)
        if not form.is_valid():
            return render(request, "donations/donation_form.html", {'program': program, 'form': form })
        donation = form.save(commit=False)
        donation.program = program
        donation.user = request.user
        donation.save()
        return redirect("donations")
    
    def get(self, request, id):
        program = get_object_or_404(Program, id=id)
        return render(request, "donations/donation_form.html", {'program': program, 'form': DonationForm() })


class DonationList(generic.ListView):
    model = Donation
    queryset = model.objects.prefetch_related("program")
    context_object_name = 'donations'
