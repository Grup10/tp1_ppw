from django.conf import settings
from django.contrib.auth.models import User
from django.utils.module_loading import import_string
from django.test import TestCase
from django.urls import reverse
from crowdfund.programs.models import Program
from crowdfund.donations.models import Donation
from crowdfund.news.models import News


class DonateTestCase(TestCase):
    def setUp(self):
        news = News.objects.create(
            title="Mahasiswa Fasilkom meng-carry temannya",
            image_url="https://files.catbox.moe/qcizhw.png",
            article_url="https://files.catbox.moe/qcizhw.png",
        )
        self.account = User.objects.create(username="haha")
        self.program = Program.objects.create(name="Donasi untuk korban PPW", news=news)
        self.donation_data = {
            "amount": Donation.minimum_donation,
            "is_anonymous": True,
        }
        self.donation_link = reverse("donate-program", kwargs={'id': self.program.id})

    def test_donation_page(self):
        self.client.force_login(self.account)
        response = self.client.get(self.donation_link)
        self.assertTemplateUsed(response, "donations/donation_form.html")

    def test_donate(self):
        self.client.force_login(self.account)
        response = self.client.post(self.donation_link, self.donation_data)
        self.assertEqual(Donation.objects.count(), 1)
        self.assertEqual(self.account.donations.count(), 1)
        self.assertEqual(response.status_code, 302)

    def test_donate_anonymous(self):
        self.test_donate()
        self.assertEqual(Donation.objects.first().display_name, "Anonymous Donor")

    def test_donate_not_anonymous(self):
        self.donation_data["is_anonymous"] = False
        self.test_donate()
        self.assertEqual(Donation.objects.first().display_name, self.account.get_full_name())

    def test_donate_invalid_amount(self):
        self.client.force_login(self.account)
        self.donation_data["amount"] = Donation.minimum_donation - 1
        response = self.client.post(self.donation_link, self.donation_data)
        self.assertEqual(Donation.objects.count(), 0)
        self.assertEqual(response.status_code, 200)

    def test_donate_not_logged_in(self):
        response = self.client.get(self.donation_link)
        self.assertEqual(response.status_code, 302)
    