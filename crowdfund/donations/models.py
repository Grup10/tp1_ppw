from django.conf import settings
from django.db import models
from django.forms import ModelForm
from django.core.validators import MinValueValidator
from django.contrib.auth.models import User

class Donation(models.Model):
    minimum_donation = 10000

    program = models.ForeignKey("programs.Program", models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="donations")
    amount = models.PositiveIntegerField(
        validators=[
            MinValueValidator(
                minimum_donation, f"The minimum donation value is Rp. {minimum_donation}"
            )
        ]
    )
    is_anonymous = models.BooleanField(verbose_name="Anonymous donation")
    date_created = models.DateTimeField(auto_now=True)

    @property
    def display_name(self):
        return "Anonymous Donor" if self.is_anonymous else self.user.get_full_name()

    class Meta:
        ordering = ["-date_created"]

