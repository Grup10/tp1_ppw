from django.apps import AppConfig


class DonateConfig(AppConfig):
    name = 'crowdfund.donations'
