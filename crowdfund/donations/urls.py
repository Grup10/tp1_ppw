from django.urls import path
from . import views

urlpatterns = [
    path("<int:id>/donate/", views.DonateNew.as_view(), name="donate-program"),
    path("", views.DonationList.as_view(), name="donations"),
]
