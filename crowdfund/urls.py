from django.urls import path, include
from .views import index, doing_some

urlpatterns = [
    path(r"", index, name="index"),
    path(r"accounts/", include("crowdfund.accounts.urls")),
    path(r"donations/", include("crowdfund.donations.urls")),
    path(r"news/", include("crowdfund.news.urls")),
    path(r"programs/", include("crowdfund.programs.urls")),
    path(r"testimony/", include("crowdfund.testimony.urls")),
    path(r"doing_some", doing_some, name="doing_some")
]
