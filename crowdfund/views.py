from django.shortcuts import render
from django.contrib.auth import logout as auth_logout
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from crowdfund.testimony.models import *
from crowdfund.testimony.forms import *


response = {}
def index(request):
	todo = Do.objects.all()
	response['todo'] = todo
	response['form'] = Testi_Form
	return render(request, "crowdfund/index.html", response)

def doing_some(request):
    form = Testi_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['testimony'] = request.POST['testimony']
        todo = Do(testimony=response['testimony'])
        todo.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')




def logout(request):
    request.session.flush()
    auth_logout(request)
    return HttpResponseRedirect('/')
