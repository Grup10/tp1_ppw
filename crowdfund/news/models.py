from django.db import models


class News(models.Model):
    title = models.CharField(max_length=100)
    image_url = models.URLField()
    article_url = models.URLField()
    date_added = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
