from django.urls import path

from . import views

urlpatterns = [
    path("news-programs", views.NewsProgramList.as_view(), name="news-programs"),
    path("", views.NewsList.as_view(), name="news"),
]
