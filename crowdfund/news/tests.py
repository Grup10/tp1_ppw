from django.test import TestCase
from .models import News


class NewsTestCase(TestCase):
    def test_news_model(self):
        news = News.objects.create(
            title="Mahasiswa Fasilkom meng-carry temannya",
            image_url="https://files.catbox.moe/qcizhw.png",
            article_url="https://files.catbox.moe/qcizhw.png",
        )
        self.assertEqual(str(news), news.title)