from django.views.generic import ListView
from .models import News


class NewsList(ListView):
    model = News
    context_object_name = 'news_list'


class NewsProgramList(ListView):
    template_name = "news/news_programs_list.html"
    model = News
    queryset = model.objects.prefetch_related("program_set")
    context_object_name = 'news_list'
