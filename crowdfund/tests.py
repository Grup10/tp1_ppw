from django.test import TestCase


class CrowdFundUrlTestCase(TestCase):
    def test_valid_urls(self):
        test_cases = (
            ("/", "crowdfund/index.html"),
            ("/news/", "news/news_list.html"),
            ("/programs/", "programs/program_list.html"),
            ("/donations/", "donations/donation_list.html")
        )

        for (url, template) in test_cases:
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, template)
