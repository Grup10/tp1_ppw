from django.shortcuts import render
from django.contrib.auth import logout, login
from django.http import HttpResponseRedirect


def home(request):
	return render(request, 'base.html')

def logoutPage(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/')