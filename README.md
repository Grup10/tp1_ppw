# TP1_PPW

Anggota Kelompok :  
1606825871 - Amira Nabila Larasati,  
1706023334 - Faza Siti Sabira Prakoso,  
1706026954 - Daffa Muhammad Rayhan,  
1706027130 - Raihan Ramadistra Pratama

Pipeline status :
[![pipeline status](https://gitlab.com/Grup10/tp1_ppw/badges/master/pipeline.svg)](https://gitlab.com/Grup10/tp1_ppw/commits/master)

Code Coverage Status :
[![coverage report](https://gitlab.com/Grup10/tp1_ppw/badges/master/coverage.svg)](https://gitlab.com/Grup10/tp1_ppw/commits/master)

Link Heroku :
https://tp-ppw1.herokuapp.com/
